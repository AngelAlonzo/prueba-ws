package cl.consorcio.apis.caja.controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.consorcio.apis.caja.domain.ValorizadaDeuda;
import cl.consorcio.apis.caja.dto.DTORequestValidarToken;
import cl.consorcio.apis.caja.res.Resources;
import cl.consorcio.apis.caja.utils.BaseDatos;
import cl.consorcio.apis.caja.utils.HttpExterno;

@RestController
public class ValorizadaDeudaController {

	private static Logger logger = LogManager.getLogger(ValorizadaDeudaController.class);

	@RequestMapping(value = "/API/V1/Leasing/ValorizadaDeuda/{ope_num}/{renta}", method = RequestMethod.GET)
	public ArrayList<ValorizadaDeuda> listarDeuda(@PathVariable("ope_num") int ope_num,
			@PathVariable("renta") int renta, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) {

		// INICIO validar token
		logger.info("Tomando valores Headers");
		String token = httpServletRequest.getHeader("token-authorization");
		String clientID = httpServletRequest.getHeader("clientID");
		String requestID = httpServletRequest.getHeader("requestID");
		String modalidad = httpServletRequest.getHeader("modalidad");
		String codigoCanal = httpServletRequest.getHeader("codigoCanal");
		String empresaAplicacion = httpServletRequest.getHeader("empresaAplicacion");
		String ipCliente = httpServletRequest.getHeader("ipCliente");
		String codigoAplicacion = httpServletRequest.getHeader("codigoAplicacion");
		DTORequestValidarToken dto = new DTORequestValidarToken();
		dto.setClienteID(clientID);
		dto.setCodigoAplicacion(codigoAplicacion);
		dto.setCodigoCanal(codigoCanal);
		dto.setEmpresaAplicacion(empresaAplicacion);
		dto.setIpCliente(ipCliente);
		dto.setModalidad(modalidad);
		dto.setRequestID(requestID);
		dto.setTokenAuthorization(token);
		// Llamado al metodo que hace la solicitud http al servicio externo
		// TODO: este metodo deberia retornar Boolean segun resultado de la validacion
		//logger.info("Validndo en HttpExterno");
		//HttpExterno.ValidarToken(dto);
		// FIN validar token*

		//if (HttpExterno.esValido()) {
			ArrayList<ValorizadaDeuda> listaValorizada = new ArrayList<ValorizadaDeuda>();
			try {
				logger.info("creando conexion a BD");
				BaseDatos bd = new BaseDatos();
				// conexion a base de datos
				logger.info("creando conexion a BD");
				Connection conexion = bd.conectar();

				CallableStatement ps = null;
				// Envio de parametros al SP
				logger.info("Envio de parametros al SP");
				String SP = Resources.getProperty("PROCEDURE_VALORIZADA_DEUDA");
				String sql = "exec " + SP + " ?,?";
				ps = conexion.prepareCall(sql);
				ps.setInt(1, ope_num);
				ps.setInt(2, renta);
				// Ejecuta Query
				logger.info("Executa Query");
				ResultSet rs = ps.executeQuery();
				logger.info("Listando Deuda");
				while (rs.next()) {
					ValorizadaDeuda valorizadaDeuda = new ValorizadaDeuda();
					valorizadaDeuda.setCli_rut(rs.getInt("cli_rut"));
					valorizadaDeuda.setCli_dv(rs.getInt("cli_dv"));
					valorizadaDeuda.setCli_nombus(rs.getString("cli_nombus"));
					valorizadaDeuda.setOpe_num(rs.getInt("ope_num"));
					valorizadaDeuda.setCot_ref(rs.getString("cot_ref"));
					valorizadaDeuda.setOr_fecven(rs.getString("or_fecven"));
					valorizadaDeuda.setOra_numrtacli(rs.getInt("Ora_numrtacli"));
					valorizadaDeuda.setMon_cod(rs.getInt("mon_cod"));
					valorizadaDeuda.setMon_abv(rs.getString("mon_abv"));
					valorizadaDeuda.setOr_valrentaiva_orig(rs.getFloat("or_valrentaiva_orig"));
					valorizadaDeuda.setOrp_intcobmo_orig(rs.getFloat("orp_intcobmo_orig"));
					valorizadaDeuda.setOrp_gtoscobmo_orig(rs.getFloat("orp_gtoscobmo_orig"));
					valorizadaDeuda.setAbono_orig(rs.getFloat("abono_orig"));
					valorizadaDeuda.setTotal_orig(rs.getFloat("total_orig"));
					valorizadaDeuda.setOr_valrentaiva_$(rs.getFloat("or_valrentaiva_$"));
					valorizadaDeuda.setOrp_intcobmo_$(rs.getFloat("orp_intcobmo_$"));
					valorizadaDeuda.setOrp_gtoscobmo_$(rs.getFloat("orp_gtoscobmo_$"));
					valorizadaDeuda.setAbono_$(rs.getFloat("abono_$"));
					valorizadaDeuda.setTC(rs.getFloat("TC"));
					valorizadaDeuda.setTotal_$(rs.getFloat("total_$"));
					valorizadaDeuda.setCod_proceso(rs.getInt("cod_proceso"));
					valorizadaDeuda.setDesc_proceso(rs.getString("desc_proceso"));

					listaValorizada.add(valorizadaDeuda);

				}
				ps.close();
				conexion.close();
				logger.info("Se realizo consulta con exito");

			} catch (Exception e) {
				System.out.println(e);
				logger.error("Error de consulta es:" + e);
			}
			//logger.error("Http autorizado");
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			logger.info("Retorna Resultado");
			return listaValorizada;
		//} else {
			//logger.error("Http no autorizado");
			//httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			//return null;
		//}
	}

}
