package cl.consorcio.apis.caja.controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.consorcio.apis.caja.domain.ConsultaDeuda;
import cl.consorcio.apis.caja.dto.DTORequestValidarToken;
import cl.consorcio.apis.caja.res.Resources;
import cl.consorcio.apis.caja.utils.BaseDatos;
import cl.consorcio.apis.caja.utils.HttpExterno;

@RestController
public class ConsultaDeudaController {

	private static Logger logger = LogManager.getLogger(ConsultaDeudaController.class);

	@RequestMapping(value = "/API/V1/Leasing/ConsultaDeuda/{cli_rut}", method = RequestMethod.GET)
	public ArrayList<ConsultaDeuda> listarTodasDeudas(@PathVariable("cli_rut") int cli_rut,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

		// INICIO validar token
		logger.info("Tomando valores Headers");
		String token =httpServletRequest.getHeader("token-authorization");
		String clienteID = httpServletRequest.getHeader("clienteID");
		String requestID = httpServletRequest.getHeader("requestID");
		String modalidad = httpServletRequest.getHeader("modalidad");
		String codigoCanal = httpServletRequest.getHeader("codigoCanal");
		String empresaAplicacion = httpServletRequest.getHeader("empresaAplicacion");
		String ipCliente = httpServletRequest.getHeader("ipCliente");
		String codigoAplicacion = httpServletRequest.getHeader("codigoAplicacion");
		DTORequestValidarToken dto = new DTORequestValidarToken();
		dto.setClienteID(clienteID);
		dto.setCodigoAplicacion(codigoAplicacion);
		dto.setCodigoCanal(codigoCanal);
		dto.setEmpresaAplicacion(empresaAplicacion);
		dto.setIpCliente(ipCliente);
		dto.setModalidad(modalidad);
		dto.setRequestID(requestID);
		dto.setTokenAuthorization(token);
		// Llamado al metodo que hace la solicitud http al servicio externo
		// TODO: este metodo deberia retornar Boolean segun resultado de la validacion
		//logger.info("Validando en HttpExterno");
		//HttpExterno.ValidarToken(dto);
		// FIN validar token*

		//if (HttpExterno.esValido()) {
			ArrayList<ConsultaDeuda> listaDeudas = new ArrayList<ConsultaDeuda>();
			try {
				logger.info("creando conexion a BD");
				BaseDatos bd = new BaseDatos();
				// conexion a base de datos
				logger.info("Estableciendo conexion a BD");
				Connection conexion = bd.conectar();
				CallableStatement ps = null;
				// Envio de parametros al SP
				logger.info("Envio de parametros al SP");
				String SP = Resources.getProperty("PROCEDURE_CONSULTA_DEUDA");
				//String sql = "exec xcash_consulta_deuda ?";
				String sql = "exec "+ SP +" ?";
				ps = conexion.prepareCall(sql);
				ps.setInt(1, cli_rut);
				// Ejecuta Query
				logger.info("Executa Query");
				ResultSet rs = ps.executeQuery();
				logger.info("Listando Deuda");
				while (rs.next()) {
					ConsultaDeuda consultaDeuda = new ConsultaDeuda();
					consultaDeuda.setCli_rut(rs.getInt("cli_rut"));
					consultaDeuda.setCli_dv(rs.getInt("cli_dv"));
					consultaDeuda.setCli_nombus(rs.getString("cli_nombus"));
					consultaDeuda.setOpe_num(rs.getInt("ope_num"));
					consultaDeuda.setCot_ref(rs.getString("cot_ref"));
					consultaDeuda.setOr_fecven(rs.getString("or_fecven"));
					consultaDeuda.setOra_numrtacli(rs.getInt("Ora_numrtacli"));
					consultaDeuda.setMon_cod(rs.getInt("mon_cod"));
					consultaDeuda.setMon_abv(rs.getString("mon_abv"));
					consultaDeuda.setOr_valrentaiva_orig(rs.getFloat("or_valrentaiva_orig"));
					consultaDeuda.setOrp_intcobmo_orig(rs.getFloat("orp_intcobmo_orig"));
					consultaDeuda.setOrp_gtoscobmo_orig(rs.getFloat("orp_gtoscobmo_orig"));
					consultaDeuda.setAbono_orig(rs.getFloat("abono_orig"));
					consultaDeuda.setTotal_orig(rs.getFloat("total_orig"));
					consultaDeuda.setOr_valrentaiva_$(rs.getFloat("or_valrentaiva_$"));
					consultaDeuda.setOrp_intcobmo_$(rs.getFloat("orp_intcobmo_$"));
					consultaDeuda.setOrp_gtoscobmo_$(rs.getFloat("orp_gtoscobmo_$"));
					consultaDeuda.setAbono_$(rs.getFloat("abono_$"));
					consultaDeuda.setTC(rs.getFloat("TC"));
					consultaDeuda.setTotal_$(rs.getFloat("total_$"));
					consultaDeuda.setCod_proceso(rs.getInt("cod_proceso"));
					consultaDeuda.setDesc_proceso(rs.getString("desc_proceso"));

					listaDeudas.add(consultaDeuda);
				}

				ps.close();
				conexion.close();
				logger.info("Se realizo consulta con exito");

			} catch (Exception e) {
				System.out.println(e);
				logger.error("Error de consulta es:" + e);
			}
			//logger.error("Http  autorizado");
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			logger.info("Retorna lista");
			return listaDeudas;
		//} else {
			//logger.error("Http no autorizado");
			//httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			//return null;
		//}
	}

}
