package cl.consorcio.apis.caja.controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.consorcio.apis.caja.domain.RecaudacionReversa;
import cl.consorcio.apis.caja.dto.DTORequestValidarToken;
import cl.consorcio.apis.caja.res.Resources;
import cl.consorcio.apis.caja.utils.BaseDatos;
import cl.consorcio.apis.caja.utils.HttpExterno;

@RestController
public class RecaudacionReversaController {

	private static Logger logger = LogManager.getLogger(RecaudacionReversaController.class);

	@RequestMapping(value = "/API/V1/Leasing/RecaudacionReversa/{ope_num}/{or_num}/{fecpro}", method = RequestMethod.GET)
	public ArrayList<RecaudacionReversa> listarReversa(@PathVariable("ope_num") int ope_num,
			@PathVariable("or_num") int or_num, @PathVariable("fecpro") String fecpro,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

		
		// INICIO validar token
		logger.info("Tomando valores Headers");
		String token = httpServletRequest.getHeader("token-authorization");
		String clientID = httpServletRequest.getHeader("clientID");
		String requestID = httpServletRequest.getHeader("requestID");
		String modalidad = httpServletRequest.getHeader("modalidad");
		String codigoCanal = httpServletRequest.getHeader("codigoCanal");
		String empresaAplicacion = httpServletRequest.getHeader("empresaAplicacion");
		String ipCliente = httpServletRequest.getHeader("ipCliente");
		String codigoAplicacion = httpServletRequest.getHeader("codigoAplicacion");
		DTORequestValidarToken dto = new DTORequestValidarToken();
		dto.setClienteID(clientID);
		dto.setCodigoAplicacion(codigoAplicacion);
		dto.setCodigoCanal(codigoCanal);
		dto.setEmpresaAplicacion(empresaAplicacion);
		dto.setIpCliente(ipCliente);
		dto.setModalidad(modalidad);
		dto.setRequestID(requestID);
		dto.setTokenAuthorization(token);
		// Llamado al metodo que hace la solicitud http al servicio externo
		// TODO: este metodo deberia retornar Boolean segun resultado de la validacion
		//logger.info("Validando en HttpExterno");
		//HttpExterno.ValidarToken(dto);
		// FIN validar token*

		//if (HttpExterno.esValido()) {
			ArrayList<RecaudacionReversa> listaReversa = new ArrayList<RecaudacionReversa>();
			try {
				logger.info("creando conexion a BD");
				BaseDatos bd = new BaseDatos();
				
				logger.info("creando conexion a BD");
				Connection conexion = bd.conectar();
				// Envio de parametros al SP
				CallableStatement ps = null;
				logger.info("Envio de parametros al SP");
				String SP = Resources.getProperty("PROCEDURE_RECAUDACION_REVERSA");
				String sql = "exec " + SP +" ?,?,?,?";
				ps = conexion.prepareCall(sql);
				ps.setInt(1, ope_num);
				ps.setInt(2, or_num);
				ps.setString(3, "'XCASH'");
				ps.setString(4, fecpro);
				// Ejecuta Query
				logger.info("Executa Query");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					RecaudacionReversa recaudacionReversa = new RecaudacionReversa();
					recaudacionReversa.setNvo_vou(rs.getInt("nvo_vou"));
					recaudacionReversa.setVou_blanco(rs.getInt("vou_blanco"));
					recaudacionReversa.setFac_num(rs.getString("fac_num"));

					listaReversa.add(recaudacionReversa);

				}

				ps.close();
				conexion.close();
				logger.info("Se realizo consulta con exito");

			} catch (Exception e) {
				System.out.println(e);
				logger.error("Error de consulta es:" + e);
			}
			//logger.error("Http autorizado");
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			logger.info("Retorna Resultado");
			return listaReversa;
		//} else {
			//logger.error("Http no autorizado");
			//httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			//return null;
		//}

	}

}
