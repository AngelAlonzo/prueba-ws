package cl.consorcio.apis.caja.controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.consorcio.apis.caja.domain.Recaudacion;
import cl.consorcio.apis.caja.domain.RecaudacionReversa;
import cl.consorcio.apis.caja.dto.DTORequestValidarToken;
import cl.consorcio.apis.caja.res.Resources;
import cl.consorcio.apis.caja.utils.BaseDatos;
import cl.consorcio.apis.caja.utils.HttpExterno;

@RestController
public class RecaudacionController {

	private static Logger logger = LogManager.getLogger(RecaudacionController.class);
	@POST
	@RequestMapping(value = "/API/V1/Leasing/Recaudacion/{ope_num}/{renta}/{cuota}/{mora}/{cobranza}/{TC}/{fecha}", method = RequestMethod.POST)
	public ArrayList<Recaudacion> ListaPago(@PathVariable("ope_num") int ope_num, @PathVariable("renta") int renta,
			@PathVariable("cuota") double cuota, @PathVariable("mora") double mora,
			@PathVariable("cobranza") double cobranza, @PathVariable("TC") double TC,
			@PathVariable("fecha") String fecha, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) {

		// INICIO validar token
		logger.info("Tomando valores Headers");
		String token = httpServletRequest.getHeader("token-authorization");
		String clientID = httpServletRequest.getHeader("clientID");
		String requestID = httpServletRequest.getHeader("requestID");
		String modalidad = httpServletRequest.getHeader("modalidad");
		String codigoCanal = httpServletRequest.getHeader("codigoCanal");
		String empresaAplicacion = httpServletRequest.getHeader("empresaAplicacion");
		String ipCliente = httpServletRequest.getHeader("ipCliente");
		String codigoAplicacion = httpServletRequest.getHeader("codigoAplicacion");
		DTORequestValidarToken dto = new DTORequestValidarToken();
		dto.setClienteID(clientID);
		dto.setCodigoAplicacion(codigoAplicacion);
		dto.setCodigoCanal(codigoCanal);
		dto.setEmpresaAplicacion(empresaAplicacion);
		dto.setIpCliente(ipCliente);
		dto.setModalidad(modalidad);
		dto.setRequestID(requestID);
		dto.setTokenAuthorization(token);
		// Llamado al metodo que hace la solicitud http al servicio externo
		// TODO: este metodo deberia retornar Boolean segun resultado de la validacion
		//logger.info("Validando en HttpExterno");
		//HttpExterno.ValidarToken(dto);
		// FIN validar token*

		//if (HttpExterno.esValido()) {
			
			
			ArrayList<Recaudacion> listaPago = new ArrayList<Recaudacion>();
			try {
				logger.info("creando conexion a BD");
				BaseDatos bd = new BaseDatos();
				
				logger.info("Estableciendo conexion a BD");
				Connection conexion = bd.conectar();
				
				CallableStatement ps = null;
				logger.info("Envio de parametros al SP");
				String SP = Resources.getProperty("PROCEDURE_RECAUDACION");
				//String sql = "exec xcash_aplica_pago ?,?,?,?,?,?,?";
				String sql = "exec " + SP + " ?,?,?,?,?,?,?";
				ps = conexion.prepareCall(sql);
				ps.setInt(1, ope_num);
				ps.setInt(2, renta);
				ps.setDouble(3, cuota);
				ps.setDouble(4, mora);
				ps.setDouble(5, cobranza);
				ps.setDouble(6, TC);
				ps.setString(7, fecha);
				
				
				logger.info("Executa Query");
				ResultSet rs = ps.executeQuery();
				logger.info("Listando Pago");
				while (rs.next()) {
					Recaudacion recaudacion = new Recaudacion();
					recaudacion.setEstado(rs.getInt("Estado"));
					recaudacion.setDescripcion(rs.getString("Descripcion"));

					listaPago.add(recaudacion);

				}
				ps.close();
				conexion.close();
				logger.info("Se realizo Pago con exito");

			} catch (Exception e) {
				System.out.println(e);
				logger.error("Error de Pago es:" + e);
			}
			//logger.error("Http autorizado");
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			logger.info("Retorna Resultado");
			return listaPago;

		//} else {
			//logger.error("Http no autorizado");
			//httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			//System.out.println("se realizo pago");
		//}
		//return null;
	}
}
