package cl.consorcio.apis.caja.domain;

public class ValorizadaDeuda {
	
	private int cli_rut;
	private int cli_dv;
	private String cli_nombus;
	private int ope_num;
	private String cot_ref;
	private String or_fecven;
	private int ora_numrtacli;
	private int mon_cod;
	private String mon_abv;
	private float or_valrentaiva_orig;
	private float orp_intcobmo_orig;
	private float orp_gtoscobmo_orig;
	private float abono_orig;
	private float total_orig;
	private float or_valrentaiva_$;
	private float orp_intcobmo_$;
	private float orp_gtoscobmo_$;
	private float abono_$;
	private float TC;
	private float total_$;
	private int cod_proceso;
	private String desc_proceso;
	
	public ValorizadaDeuda() {
		
	}

	public int getCli_rut() {
		return cli_rut;
	}

	public void setCli_rut(int cli_rut) {
		this.cli_rut = cli_rut;
	}

	public int getCli_dv() {
		return cli_dv;
	}

	public void setCli_dv(int cli_dv) {
		this.cli_dv = cli_dv;
	}

	public String getCli_nombus() {
		return cli_nombus;
	}

	public void setCli_nombus(String cli_nombus) {
		this.cli_nombus = cli_nombus;
	}

	public int getOpe_num() {
		return ope_num;
	}

	public void setOpe_num(int ope_num) {
		this.ope_num = ope_num;
	}

	public String getCot_ref() {
		return cot_ref;
	}

	public void setCot_ref(String cot_ref) {
		this.cot_ref = cot_ref;
	}

	public String getOr_fecven() {
		return or_fecven;
	}

	public void setOr_fecven(String or_fecven) {
		this.or_fecven = or_fecven;
	}

	public int getOra_numrtacli() {
		return ora_numrtacli;
	}

	public void setOra_numrtacli(int ora_numrtacli) {
		this.ora_numrtacli = ora_numrtacli;
	}

	public int getMon_cod() {
		return mon_cod;
	}

	public void setMon_cod(int mon_cod) {
		this.mon_cod = mon_cod;
	}

	public String getMon_abv() {
		return mon_abv;
	}

	public void setMon_abv(String mon_abv) {
		this.mon_abv = mon_abv;
	}

	public float getOr_valrentaiva_orig() {
		return or_valrentaiva_orig;
	}

	public void setOr_valrentaiva_orig(float or_valrentaiva_orig) {
		this.or_valrentaiva_orig = or_valrentaiva_orig;
	}

	public float getOrp_intcobmo_orig() {
		return orp_intcobmo_orig;
	}

	public void setOrp_intcobmo_orig(float orp_intcobmo_orig) {
		this.orp_intcobmo_orig = orp_intcobmo_orig;
	}

	public float getOrp_gtoscobmo_orig() {
		return orp_gtoscobmo_orig;
	}

	public void setOrp_gtoscobmo_orig(float orp_gtoscobmo_orig) {
		this.orp_gtoscobmo_orig = orp_gtoscobmo_orig;
	}

	public float getAbono_orig() {
		return abono_orig;
	}

	public void setAbono_orig(float abono_orig) {
		this.abono_orig = abono_orig;
	}

	public float getTotal_orig() {
		return total_orig;
	}

	public void setTotal_orig(float total_orig) {
		this.total_orig = total_orig;
	}

	public float getOr_valrentaiva_$() {
		return or_valrentaiva_$;
	}

	public void setOr_valrentaiva_$(float or_valrentaiva_$) {
		this.or_valrentaiva_$ = or_valrentaiva_$;
	}

	public float getOrp_intcobmo_$() {
		return orp_intcobmo_$;
	}

	public void setOrp_intcobmo_$(float orp_intcobmo_$) {
		this.orp_intcobmo_$ = orp_intcobmo_$;
	}

	public float getOrp_gtoscobmo_$() {
		return orp_gtoscobmo_$;
	}

	public void setOrp_gtoscobmo_$(float orp_gtoscobmo_$) {
		this.orp_gtoscobmo_$ = orp_gtoscobmo_$;
	}

	public float getAbono_$() {
		return abono_$;
	}

	public void setAbono_$(float abono_$) {
		this.abono_$ = abono_$;
	}

	public float getTC() {
		return TC;
	}

	public void setTC(float tC) {
		TC = tC;
	}

	public float getTotal_$() {
		return total_$;
	}

	public void setTotal_$(float total_$) {
		this.total_$ = total_$;
	}

	public int getCod_proceso() {
		return cod_proceso;
	}

	public void setCod_proceso(int cod_proceso) {
		this.cod_proceso = cod_proceso;
	}

	public String getDesc_proceso() {
		return desc_proceso;
	}

	public void setDesc_proceso(String desc_proceso) {
		this.desc_proceso = desc_proceso;
	}	

}
