package cl.consorcio.apis.caja.domain;

public class RecaudacionReversa {
	

    private int nvo_vou;
    private int vou_blanco;
    private String fac_num;
    
    public RecaudacionReversa() {
    	
    }

	public String getFac_num() {
		return fac_num;
	}

	public void setFac_num(String fac_num) {
		this.fac_num = fac_num;
	}

	public int getNvo_vou() {
		return nvo_vou;
	}

	public void setNvo_vou(int nvo_vou) {
		this.nvo_vou = nvo_vou;
	}

	public int getVou_blanco() {
		return vou_blanco;
	}

	public void setVou_blanco(int vou_blanco) {
		this.vou_blanco = vou_blanco;
	}


    
}
