package cl.consorcio.apis.caja.utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jndi.JndiTemplate;

import cl.consorcio.apis.caja.res.Resources;
import cl.consorcio.apis.caja.utils.BaseDatos;

public class BaseDatos {
	
	private static Logger logger =LogManager.getLogger(BaseDatos.class);
	
	static String bd = "LEASING";
	static String login = "sa";
	static String password = "123456";
	static String url = "jdbc:sqlserver://W7-PC:1433;databaseName=LEASING";
	Connection conexion = null;
			/* "W7-PC(SQL server 11.0.2100 - W7-PC\W7";*/
	
	//@Autowired
	//Environment env;
	
	public Connection conectar(){
		//Connection conexion = null;
		//JndiTemplate jndi = new JndiTemplate();
		
		
		try {
			
			   	logger.info("Ejecutando driver JDBC");
			   	//String DATASOURCE = Resources.getProperty("DATASOURCE_SQLSERVER_URL");
			   	//String DATASOURCE = Resources.getProperty("DATASOURCE_SQLSERVER_ARY");
			   	String DATASOURCE = Resources.getProperty("DATASOURCE_SQLSERVER");
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				
				//conexion = DriverManager.getConnection("jdbc:sqlserver://DESKTOP-7K52NH4\\SQL2012:1433;databaseName=LEASING","sa", "123456");
				//logger.info("Ejecutando resources");
				//conexion = DriverManager.getConnection("jdbc:sqlserver://W7-PC:1433;databaseName=LEASING", "sa","123456");
				//conexion = DriverManager.getConnection(DATASOURCE, login, password);
				//Context ctx = new InitialContext();
				logger.info("Ejecutando driver Jndi");
				JndiTemplate jndi = new JndiTemplate();
				//env.getProperty("nombre");
				logger.info("Lookup Conexion DataSource");
				//DataSource Jboss
				//DataSource dataSource = jndi.lookup("java:/jboss/datasources/LEASOFT",DataSource.class);
				DataSource dataSource = jndi.lookup(DATASOURCE,DataSource.class);
				//DataSource Tomcat
				//DataSource dataSource = jndi.lookup("java:/comp/env/jdbc/LEASOFT",DataSource.class);
			
				
				
				
				logger.info("Conexion DataSource");
				conexion = dataSource.getConnection();
				
				//Context initContext = new InitialContext();
		      //DataSource dataSource = (DataSource)initContext.lookup("java:/comp/env/jdbc/LEASING");

				if (conexion!=null) {
					logger.info("Conexion a base de datos correctamente");
				}
				
		}catch(Exception e){
			System.out.println("Error" + e);
			logger.error("Error es:" +  e);
		}
		return conexion;
	}
	


}

