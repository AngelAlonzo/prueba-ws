package cl.consorcio.apis.caja.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import cl.consorcio.apis.caja.controller.ConsultaDeudaController;
import cl.consorcio.apis.caja.dto.DTORequestValidarToken;
import cl.consorcio.apis.caja.dto.Respuesta;

public class HttpExterno {
	
	private static Logger logger = LogManager.getLogger(HttpExterno.class);
	//URL del servicio externo para validar Token
	private static final String TOKEN_URL = "http://10.250.12.175:8080/BCNSWSR_TOKENGEN/Token/Consultar";
	//Timeout para la conexion
	private static int TIMEOUT = 30000;
	//Metodo para validar Token
	//TODO: este metodo deberia retornar Boolean segun resultado de la validacion

	private static boolean esValido = false;
	
	public static void ValidarToken(DTORequestValidarToken dto){
		try {
			logger.info("Cargando URL");
			URL url = new URL(TOKEN_URL);
			//Crear la conexion
			logger.info("Crear la conexion");
			HttpURLConnection conexion = (HttpURLConnection)url.openConnection();
			//Metodo POST para la solicitud
			conexion.setRequestMethod("POST");
			//Colocar Access Token en el header
			logger.info("Colocando Access Token en el header");
			conexion.addRequestProperty("X-Auth-Token", dto.getTokenAuthorization()); //Token dentro de header
			//Asignar un valor de timeout
			logger.info("Asignando valor al TIMEOUT");
			conexion.setConnectTimeout(TIMEOUT);
			conexion.setReadTimeout(TIMEOUT);
			logger.info("Parametros de entrada");
			//Parametros de entrada, como pares clave-valor (HashMap)
			Map<String, String> params = new HashMap<>();
			params.put("clientid", dto.getClienteID());
			params.put("requestID", dto.getRequestID());
			params.put("modalidad", dto.getModalidad());
			params.put("codigoCanal", dto.getCodigoCanal());
			params.put("empresaAplicacion", dto.getEmpresaAplicacion());
			params.put("ipCliente", dto.getIpCliente());
			params.put("token-authorization", dto.getTokenAuthorization());
			params.put("codigoAplicacion", dto.getCodigoAplicacion());
			//Habilitar parametros
			logger.info("Habilitar parametros");
			conexion.setDoOutput(true);
			logger.info("Colocando parametros del request");
			//Con este objeto se colocan los parametros del request
			DataOutputStream out = new DataOutputStream(conexion.getOutputStream());
			//Escribir los datos formateados con la funcion abajo
			logger.info("Escribir datos formateados");
			out.writeBytes(getQueryString(params));
			//Limpiar el OutputStream
			logger.info("Limpiar el OutputStream");
			out.flush();
			//Cerrar al finalizar la escritura
			logger.info("Cerrar al finalizar la escritura");
			out.close();
			//Enviar la solicitud y leer respuesta
			logger.info("Envio de la solicitud y leer respuesta");
			int status = conexion.getResponseCode(); //Generalmente status = 200 es OK
			//Leer entrada como texto
			logger.info("Leer entrada como texto");
			BufferedReader in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
			logger.info("linea");
			String linea;
			logger.info("crear SringBuffer");
			StringBuffer respuesta = new StringBuffer();
			logger.info("While entrada");
			while ((linea = in.readLine()) != null) {
				respuesta.append(linea);
			}
			logger.info("Convertir objeto toString");
			ObjectMapper objectMapper = new ObjectMapper();
			Respuesta respuesta1  = objectMapper.readValue(respuesta.toString().getBytes(), Respuesta.class);
			
			logger.info("Respuestas del Header");
			//if(respuesta1.getDtoResponseCodigoEstado().getCodigo() == "0");
			logger.info("ResponseCodigoEstado = 0");
			if(respuesta1.getRespuestaConsultaToken().getMensaje().equals("TOKEN VALIDO"));
			logger.info("Token es VALIDO");
			
			logger.info("esValido = true");
			esValido = true;
			
			//Despues de aca, se podria convertir respuesta.toString()
			//y desde alli parsear la respuesta JSON (librerias GSON o Jackson)
			//o XML
			
			//Cerrar inputStream
			in.close();
		}
		catch(Exception e) {
			System.out.println(e);
			logger.error("Error HTTP:" + e);
			esValido = false;
		}
		//return ;
	}
	//Funcion para convertir de map a un string de entrada en la forma
	//param1=valor1&param2=valor2&...&paramN=valorN
	private static String getQueryString(Map<String, String> params) 
      throws Exception{
        StringBuilder sb = new StringBuilder();
        logger.info("convertir map");
        try {
        for (Map.Entry<String, String> entry : params.entrySet()) {
          logger.info("convertir map -get key");
          sb.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
          sb.append("=");
          logger.info("convertir map -get value");
          sb.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
          sb.append("&");
        }
        }
		catch(IOException e) {
			System.out.println(e);
			logger.error("Error covert MaP:" + e);

		}
        logger.info("Crae String resultado");
        String resultado = sb.toString();
        logger.info("retorna String resultado");
        return resultado.length() > 0
          ? resultado.substring(0, resultado.length() - 1)
          : resultado;
    }
       
	public static boolean esValido(){
		logger.info("retorna esValido");
		return esValido;
	}
}
