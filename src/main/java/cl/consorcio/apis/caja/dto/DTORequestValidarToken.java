package cl.consorcio.apis.caja.dto;

public class DTORequestValidarToken {
	private String clienteID;
	private String requestID;
	private String modalidad;
	private String codigoCanal;
	private String empresaAplicacion;
	private String ipCliente;
	private String tokenAuthorization;
	private String codigoAplicacion;
	
	public String getClienteID() {
		return clienteID;
	}
	public void setClienteID(String clienteID) {
		this.clienteID = clienteID;
	}
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	public String getModalidad() {
		return modalidad;
	}
	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}
	public String getCodigoCanal() {
		return codigoCanal;
	}
	public void setCodigoCanal(String codigoCanal) {
		this.codigoCanal = codigoCanal;
	}
	public String getEmpresaAplicacion() {
		return empresaAplicacion;
	}
	public void setEmpresaAplicacion(String empresaAplicacion) {
		this.empresaAplicacion = empresaAplicacion;
	}
	public String getIpCliente() {
		return ipCliente;
	}
	public void setIpCliente(String ipCliente) {
		this.ipCliente = ipCliente;
	}
	public String getTokenAuthorization() {
		return tokenAuthorization;
	}
	public void setTokenAuthorization(String tokenAuhorization) {
		this.tokenAuthorization = tokenAuhorization;
	}
	public String getCodigoAplicacion() {
		return codigoAplicacion;
	}
	public void setCodigoAplicacion(String codigoAplicacion) {
		this.codigoAplicacion = codigoAplicacion;
	}
}
