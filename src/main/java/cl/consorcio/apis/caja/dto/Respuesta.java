package cl.consorcio.apis.caja.dto;

public class Respuesta {
	
	private ObjetoRespuesta dtoResponseCodigoEstado;
	private RespuestaConsultaToken respuestaConsultaToken;
	public ObjetoRespuesta getDtoResponseCodigoEstado() {
		return dtoResponseCodigoEstado;
	}
	public void setDtoResponseCodigoEstado(ObjetoRespuesta dtoResponseCodigoEstado) {
		this.dtoResponseCodigoEstado = dtoResponseCodigoEstado;
	}
	public RespuestaConsultaToken getRespuestaConsultaToken() {
		return respuestaConsultaToken;
	}
	public void setRespuestaConsultaToken(RespuestaConsultaToken respuestaConsultaToken) {
		this.respuestaConsultaToken = respuestaConsultaToken;
	}
	
	

}
