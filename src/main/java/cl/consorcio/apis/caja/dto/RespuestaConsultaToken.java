package cl.consorcio.apis.caja.dto;

public class RespuestaConsultaToken {

	private String mensaje;
	private String minutosRestantesParaExpirar;
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getMinutosRestantesParaExpirar() {
		return minutosRestantesParaExpirar;
	}
	public void setMinutosRestantesParaExpirar(String minutosRestantesParaExpirar) {
		this.minutosRestantesParaExpirar = minutosRestantesParaExpirar;
	}
	
	
}
