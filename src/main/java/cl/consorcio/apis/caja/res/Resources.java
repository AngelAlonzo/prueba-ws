package cl.consorcio.apis.caja.res;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Resources {
	
	private static Logger logger =LogManager.getLogger(Resources.class);
	
	public static Properties getPropiedades(){
		
		Properties p = new Properties();
		try {
		
		logger.info("lee archivo properties");
		//InputStream is = new FileInputStream("C:/Users/Desarrollo/Desktop/BCNSWSR_Cajas/src/main/resources/BCNSWSR_Cajas-resource.properties");
		//InputStream is = new FileInputStream(".\\BCNSWSR_Cajas\\src\\main\\resources\\application.properties");
		InputStream is = new FileInputStream("/opt/jboss-eap-7.0/modules/fisa/main/properties/BCNSWSR_Cajas-resource.properties");
		p.load(is);
		}
		catch(Exception e){
			System.out.println("Error" + e);
			logger.error("Error en carga properties es:" +  e);
		}
		logger.info("retorna resultados");
		return p;
	}
	
	public static String getProperty(String nombrePropiedad) {
		logger.info("entra a metodo getProperty");
		return getPropiedades().getProperty(nombrePropiedad);
		
	}
	
}
