package cl.consorcio.apis.caja.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "cl.consorcio.apis.caja")
public class HelloWorldConfiguration {
	

}